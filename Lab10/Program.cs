﻿using System;
using _Lab10.Entities;
using System.Collections.Generic;
using System.Reflection;

namespace _Lab10
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee>();

            employees.Add(new Employee("Andrey", 89, true));
            employees.Add(new Employee("Masha", 18, true));
            employees.Add(new Employee("Sasha", 16, false));
            employees.Add(new Employee("Someone", 90, false));
            employees.Add(new Employee("Tomas", 31, true));

            string dllFile = "/home/robot/code/csharp/FileServiceLibrary/bin/Debug/net6.0/FileServiceLibrary.dll";
            var assembly = Assembly.LoadFile(dllFile);

            var type = assembly.GetType("FileServiceLibrary.FileService`1", true).MakeGenericType(typeof(Employee));

            var obj = Activator.CreateInstance(type);
            var SaveData = type.GetMethod("SaveData");
            var ReadFile = type.GetMethod("ReadFile");
            SaveData.Invoke(obj, new object[] { employees, "employees.json" });
            var result = (List<Employee>)ReadFile.Invoke(obj, new object[] { "employees.json" });
            foreach (var employee in result)
            {
                Console.WriteLine(employee.Name);
                Console.WriteLine($"    {employee.Age}");
                Console.WriteLine($"    {employee.Work}");
            }
        }
    }
}
