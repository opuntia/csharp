using System.Collections.Generic;

namespace _Lab10.Entities
{
    class Employee
    {
        private int _age;
        private bool _work;
        private string _name;

        public string Name { get => _name; set => _name = value; }
        public int Age { get => _age; set => _age = value; }
        public bool Work { get => _work; set => _work = value; }

        public Employee(string name, int age, bool work)
        {
            _name = name;
            _age = age;
            _work = work;
        }
    }
}