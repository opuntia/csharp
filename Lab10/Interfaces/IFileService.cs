using System;
using System.Collections.Generic;
using _Lab10.Entities;

namespace _Lab10.Interfaces
{
    public interface IFileService<T> where T:class
    {
        IEnumerable<T> ReadFile(string filename);
        void SaveData(IEnumerable<T> data, string filename);
    }
}