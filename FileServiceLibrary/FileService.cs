using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using _Lab10.Interfaces;

namespace FileServiceLibrary
{
    public class FileService<T> : IFileService<T> where T : class
    {
        public IEnumerable<T> ReadFile(string fileName)
        {
            string json = File.ReadAllText(fileName);
            return JsonSerializer.Deserialize<List<T>>(json);
        }

        public void SaveData(IEnumerable<T> data, string fileName)
        {
            var options = new JsonSerializerOptions { WriteIndented = true };
	        var json = JsonSerializer.Serialize(data, options);
			File.WriteAllText(fileName, json);
        }
    }
}
