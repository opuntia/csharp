using System.Collections.Generic;

namespace Airports
{
	public interface ISerializer
	{
		IEnumerable<Airport> DeSerializeByLINQ(string fileName);
		IEnumerable<Airport> DeSerializeXML(string fileName);
		IEnumerable<Airport> DeSerializeJSON(string fileName);
		void SerializeByLINQ(IEnumerable<Airport> airports, string fileName);
		void SerializeXML(IEnumerable<Airport> airports, string fileName);
		void SerializeJSON(IEnumerable<Airport> airports, string fileName);
	}
}
