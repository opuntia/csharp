﻿using System.Collections.Generic;

namespace Airports
{
    public class Airport
    {
		private string _name;
		private List<Runway> _runways;

		public string Name { get => _name; set => _name = value; }
		public List<Runway> Runways { get => _runways; set => _runways = value; }

		public Airport(string name)
		{
			_name = name;
			_runways = new List<Runway>();
		}

        public Airport(string name, List<Runway> runways)
		{
			_name = name;
			_runways = runways;
		}

		public Airport(){}

		public void AddRunway(Runway runway)
		{
			_runways.Add(runway);
		}

		public void RemoveRunway(string name)
		{
			Runway runway = _runways.Find(r => r.Name == name);
			_runways.Remove(runway);
		}
    }
}
