﻿using System;

namespace Airports
{
    public class Runway
    {
		private string _name;
		private int _length;

		public string Name { get => _name; set => _name = value; }
		public int Length { get => _length; set => _length = value; }

		public Runway(string name, int length)
		{
			_name = name;
			_length = length;
		}

		public Runway(){}
    }
}
