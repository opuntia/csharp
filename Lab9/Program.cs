﻿using System;
using Airports;
using Serializers;
using System.Collections.Generic;

namespace _Lab9
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Airport> airports = new List<Airport>();
            List<Airport> airportsFromJson = new List<Airport>();
            List<Airport> airportsFromXML = new List<Airport>();
            List<Airport> airportsFromLINQToXML = new List<Airport>();

            Airport minsk = new Airport("Minsk");
            Airport usa = new Airport("USA");
            Airport germany = new Airport("Germany");
            Airport russia = new Airport("Russia");
            Airport smth = new Airport("Smth");
            Airport smne = new Airport("Smne");

            minsk.AddRunway(new Runway("blabla", 134));
            minsk.AddRunway(new Runway("qwerty", 178));

            usa.AddRunway(new Runway("lasdkjf", 198));
            usa.AddRunway(new Runway("lasjdlfjsad", 898));

            airports.Add(minsk);
            airports.Add(usa);
            airports.Add(germany);
            airports.Add(russia);
            airports.Add(smth);
            airports.Add(smne);

            Serializer serializer = new Serializer();
            serializer.SerializeJSON(airports, "airportsJSON.json");
            airportsFromJson = (List<Airport>)serializer.DeSerializeJSON("airportsJSON.json");
            Console.WriteLine("JSON desirialize:");
            foreach (var airport in airportsFromJson)
            {
                Console.WriteLine(airport.Name);
                foreach (var runway in airport.Runways)
                {
                    Console.WriteLine($"  {runway.Name}");
                    Console.WriteLine($"    {runway.Length}");
                }
            }


            serializer.SerializeXML(airports, "airportsXML.xml");
            airportsFromXML = (List<Airport>)serializer.DeSerializeXML("airportsXML.xml");

            Console.WriteLine("\nXML desirialize:");
            foreach (var airport in airportsFromXML)
            {
                Console.WriteLine(airport.Name);
                foreach (var runway in airport.Runways)
                {
                    Console.WriteLine($"  {runway.Name}");
                    Console.WriteLine($"    {runway.Length}");
                }
            }

            serializer.SerializeByLINQ(airports, "airportsByLINQ.xml");
            airportsFromLINQToXML = (List<Airport>)serializer.DeSerializeByLINQ("airportsByLINQ.xml");

            Console.WriteLine("\nBy LINQ desirialize:");
            foreach (var airport in airportsFromLINQToXML)
            {
                Console.WriteLine(airport.Name);
                foreach (var runway in airport.Runways)
                {
                    Console.WriteLine($"  {runway.Name}");
                    Console.WriteLine($"    {runway.Length}");
                }
            }



        }
    }
}
