﻿using System.Diagnostics;

namespace Integral
{
    public class IntegralCalc
    {
        public event EventHandler<IntegralEventArgs> OnIntegralCalc;

        public void Calculate()
        {
            var sw = new Stopwatch();
            sw.Start();
            double result = 0.0;
            double step = 0.00000001;
            for (double i = 0.0; i <= 1.0; i += step)
            {
                result += Math.Sin(i);
            }
            result = result * step;
            sw.Stop();
            Console.WriteLine($"Time spent: {sw.ElapsedMilliseconds}");
            OnIntegralCalc?.Invoke(this, new IntegralEventArgs { result = result });
        }

    }

    public class IntegralEventArgs : EventArgs
    {
        public double result;
    }
}
