﻿﻿using System.Text.Json;
using Toys;
using System;
using System.IO;
using System.Text;

namespace StreamServices;

public class StreamService
{
    private static object locker = new object();

    public Task WriteToStream(Stream stream)
    {
        return Task.Run(() => {
            lock (locker)
            {
                Console.WriteLine($"write to stream started at {Thread.CurrentThread.ManagedThreadId} thread");
                var options = new JsonSerializerOptions { WriteIndented = true };
                List<Toy> toys = new List<Toy>();
                var rand  = new Random();
                for (int i=0; i < 100; i++)
                {
                    bool rand_bool = (rand.NextDouble() > 0.5);
                    var toy = new Toy { Name = $"toy{i}", AgeEqualOrGreaterThanSix = rand_bool };
                    toys.Add(toy);
                }
                var json = JsonSerializer.Serialize(toys, options);
                byte[] array = Encoding.Default.GetBytes(json);
                stream.Write(array);
            }
            Console.WriteLine($"write to stream ended at {Thread.CurrentThread.ManagedThreadId} thread");
        });
    }

    public Task CopyFromStream(string filename, Stream stream)
    {
        return Task.Run(() => {
            lock (locker)
            {
                Console.WriteLine($"write from stream to file started at {Thread.CurrentThread.ManagedThreadId} thread");
                if (stream.Length == 0)
                    throw new Exception("stream is empty");
                using (FileStream fs = File.Create(filename))
                {
                    stream.Seek(0, SeekOrigin.Begin);
                    stream.CopyTo(fs);
                }
            }
            Console.WriteLine($"write from stream to file ended at {Thread.CurrentThread.ManagedThreadId} thread");
        });
    }

    public async Task<int> GetStatisticsAsync(string filename, Func<Toy, bool> filter)
    {
        var count = 0;
        Console.WriteLine($"read from stream with filter started at {Thread.CurrentThread.ManagedThreadId} thread");

        using (FileStream fs = new FileStream(filename, FileMode.Open))
        {
            List<Toy>? restoredToys = await JsonSerializer.DeserializeAsync<List<Toy>>(fs);
            foreach (var toy in restoredToys)
            {
                if (filter(toy))
                    count++;
            }
        }

        Console.WriteLine($"read from file with filter ended at {Thread.CurrentThread.ManagedThreadId} thread");
        return count;
    }

}
