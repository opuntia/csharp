﻿using System.Text.Json;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Airports;

namespace Serializers
{
    public class Serializer : ISerializer
    {
		public IEnumerable<Airport> DeSerializeByLINQ(string fileName)
		{
            XElement root = XElement.Load(fileName);
            return root.Descendants("airport").Select(airport => new Airport(
									airport.Attribute("name").Value.ToString(),
									airport.Descendants("runway").Select(runway => new Runway(
													runway.Attribute("name").Value.ToString(),
													System.Convert.ToInt32(runway.Element("length").Value)
												)).ToList()
									)).ToList();
		}

		public IEnumerable<Airport> DeSerializeXML(string fileName)
		{
            XmlSerializer formatter = new XmlSerializer(typeof(List<Airport>));

            using (FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                return (IEnumerable<Airport>)formatter.Deserialize(fs);
            }
		}

		public IEnumerable<Airport> DeSerializeJSON(string fileName)
		{
            string json = File.ReadAllText(fileName);
            return JsonSerializer.Deserialize<List<Airport>>(json);
		}

		public void SerializeByLINQ(IEnumerable<Airport> airports, string fileName)
		{
            XDocument doc = new XDocument();
			XElement airportsElement = new XElement("airports");
            foreach (Airport airport in airports)
            {
                XElement runwaysElement = new XElement("runways");

                foreach (Runway runway in airport.Runways)
                {
                    runwaysElement.Add(new XElement("runway", new XAttribute("name", runway.Name),
												 			  new XElement("length", runway.Length)));
                }

                airportsElement.Add(new XElement("airport",
                    new XAttribute("name", airport.Name),
					runwaysElement
                ));
            }
			doc.Add(airportsElement);
			doc.Save(fileName);
		}

		public void SerializeXML(IEnumerable<Airport> airports, string fileName)
		{
            XmlSerializer formatter = new XmlSerializer(typeof(List<Airport>));

            using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
               formatter.Serialize(fs, airports);
            }
		}

		public void SerializeJSON(IEnumerable<Airport> airports, string fileName)
		{
            var options = new JsonSerializerOptions { WriteIndented = true };
	        var json = JsonSerializer.Serialize(airports, options);
			File.WriteAllText(fileName, json);
		}
    }
}
