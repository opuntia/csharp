﻿using System;

namespace Toys;
public class Toy
{
    private string _uuid;
    private string _name;
    private bool _ageEqualOrGreaterThanSix;

    public Toy()
    {
        _uuid = Guid.NewGuid().ToString("N");
    }

    public string Name { get => _name; set => _name = value; }
    public string Uuid { get => _uuid; }
    public bool AgeEqualOrGreaterThanSix { get => _ageEqualOrGreaterThanSix;
                                           set => _ageEqualOrGreaterThanSix = value; }

}
