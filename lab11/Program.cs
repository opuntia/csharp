﻿using System.Diagnostics;
using Integral;
using Toys;
using StreamServices;
using System.IO;
using System.Text;

class Programm
{
    static void PrintResult(object? sender, IntegralEventArgs e)
    {
        Console.WriteLine($"result: {e.result}");
    }

    static bool filter(Toy toy)
    {
        return toy.AgeEqualOrGreaterThanSix;
    }

    static async Task Main(string[] args)
    {
        // task 1
        var integral = new IntegralCalc();
        integral.OnIntegralCalc += PrintResult;

        integral.Calculate();

        // task 2
        string filename = "toys.json";
        var ss = new StreamService();

        using (MemoryStream fs = new MemoryStream())
        {
            var t = ss.WriteToStream(fs);
            var t1 = ss.CopyFromStream(filename, fs);
            t1.Wait();
            t.Wait();
        }
        int count = await ss.GetStatisticsAsync(filename, filter);
        Console.WriteLine(count);
    }
}
